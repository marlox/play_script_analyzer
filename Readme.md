# PLAY SCRIPT ANALYZER

As a Shakespeare buff, statistics junkie, and Unix lover, Ben finds himself wanting a command-line tool for analyzing Macbeth.
Write a command-line program that prints the number of lines spoken by each character in the play.

Setup instructions
* Setup the repository:

```shell
$ git clone https://gitlab.com/marlox/play_script_analyzer.git
$ cd play_script_analyzer
$ bundle
```

Sample usage/output (using made-up numbers):

```shell
$ ruby script_analyzer.rb
543 Macbeth
345 Banquo
220 Duncan
(etc.)
```

* You can find an XML-encoded version of Macbeth here: http://www.ibiblio.org/xml/examples/shakespeare/macbeth.xml.
* Your program should download and parse this file at runtime.
* Your solution should be tested, preferably via TDD. Running your tests should not download the play from the ibiblio.org server.
* Some lines are attributed to a speaker called "ALL". Your program should ignore these.

# OTHER PLAY SCRIPT
You can also analyze other play script by passing a valid URL:

```shell
$ ruby script_analyzer.rb http://www.ibiblio.org/xml/examples/shakespeare/coriolan.xml
```

# TESTING
If you wish to run RSPEC to test this (if you haven't installed rspec yet), run these commands:

```shell
$ gem install rspec
$ rspec spec
```
