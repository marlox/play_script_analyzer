require_relative 'lib/play_script_analyzer'

play_script_path = ARGV[0] || nil

analyzer = PlayScriptAnalyzer.new download: true, url: play_script_path
analyzer.run print: true
