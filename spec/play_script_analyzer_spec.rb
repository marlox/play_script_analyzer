require 'spec_helper'

describe PlayScriptAnalyzer do
  context 'When analyzing Shakespeare Play "The Tragedy of Macbeth"' do

    def analyzer
      PlayScriptAnalyzer.new
    end

    it 'should retrieve the number of script lines per character' do
      analyzer.run.each do |name,result|
        expect(result).to have_key :character
        expect(result).to have_key :lines
        expect(result[:lines]).to be_between 1, 1000
      end
    end

    it 'should NOT retrieve the number of script lines per character if passed an argument :print' do
      script_lines = analyzer.run print: true

      expect(script_lines).to be_nil
    end

  end
end
