require 'spec_helper'

describe PlayScript do
  context 'When opening the Shakespeare Play from Ibiblio.org' do

    def play_script
      PlayScript.new
    end

    it 'should have a title "The Tragedy of Macbeth"' do
      expect(play_script.title).to eq 'The Tragedy of Macbeth'
    end

    it 'should retrieve the number of script lines for a character' do
      name = 'MACBETH'
      result = play_script.for_character(name)

      expect(result).to have_key :character
      expect(result).to have_key :lines

      result = result[:character]
      expect(result).to eq name
    end

    it 'should retrieve script lines per character' do
      result = play_script.character_lines

      expect(result).to be_a_kind_of Hash
      expect(result.length).to be_between 1, 100
    end

  end

  context 'When wrong URL used in opening the Shakespeare Play from Ibiblio.org' do

    def play_script
      PlayScript.new url: 'http://www.ibiblio.org/macbeth.xml'
    end

    it 'should NOT have a title "The Tragedy of Macbeth"' do
      expect(play_script.title).to be_nil
    end

    it 'should retrieve the number of script lines for a character' do
      result = play_script.for_character('MACBETH')

      expect(result).to be_nil
    end

    it 'should NOT retrieve script lines per character' do
      expect(play_script.character_lines).to be_nil
    end

  end
end
