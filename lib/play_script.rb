require 'nokogiri'
require 'open-uri'

class PlayScript

  URL = 'http://www.ibiblio.org/xml/examples/shakespeare/macbeth.xml'

  attr_accessor :script, :filename

  def initialize opts={}
    is_download = opts[:download] || false
    url = opts[:url] || URL

    @script = is_download ? download(url) : read(url)

    self
  end

  def download file
    @filename = file.split('/').last

    IO.copy_stream(open(file), @filename) unless File.exists?(@filename)

    File.open(@filename)

  rescue OpenURI::HTTPError
    puts "Invalid file source for play script - #{file}"
  end

  def read url
    script = open(url)

    @filename = script.base_uri.to_s.split('/').last

    script

  rescue OpenURI::HTTPError
    puts "Invalid file source for play script - #{url}"
  end

  def title
    return @script unless @script

    Nokogiri::XML(@script).at_css('PLAY TITLE').text
  end

  def for_character name
    lines = character_lines || {}
    lines[name.to_s.downcase]
  end

  def character_lines
    script_lines = nil

    if @script
      script_lines = {}
      Nokogiri::XML(@script).css('ACT SCENE SPEECH').each do |speech|
        speaker = speech.at_css('SPEAKER').text
        unless speaker.downcase['all']
          script_lines[speaker.downcase] ||= { character: speaker, lines: 0 } 
          script_lines[speaker.downcase][:lines] += speech.css('LINE').length
        end
      end
    end

    script_lines
  end

end
