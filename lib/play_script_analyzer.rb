require 'nokogiri'
require_relative 'play_script'

class PlayScriptAnalyzer

  attr_accessor :play

  def initialize opts={}
    @play = PlayScript.new(opts)
  end

  def run opts={}
    lines = @play.character_lines || {}

    if opts[:print]
      lines.each{ |key,line| puts "#{line[:lines]} #{line[:character]}" }
      nil
    else
      lines
    end
  end

end
